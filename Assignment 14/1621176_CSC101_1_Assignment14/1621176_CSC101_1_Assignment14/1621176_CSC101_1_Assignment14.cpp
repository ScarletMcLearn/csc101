//
//  main.cpp
//  1621176_CSC101_1_Assignment14
//
//  Created by Ratul's Mac on 6/6/16.
//  Copyright © 2016 Ratul's Mac. All rights reserved.
//

#include <iostream>
using namespace std;


int main()
{
    int n,i,j,k;
    cout<<"Please Enter The Height Of Triangle : ";
    cin>> n;
 
    for(i=0;i<n;i++){
        for(j=0;j<n-i-1;j++){
            cout<<" ";
        }
        for(k=0;k<i;k++){
            cout<<"*";
        }
        cout<<("\n");
    }
}

//int main(int argc, const char * argv[])
//{
//    // insert code here...
//    std::cout << "Hello, World!\n";
//    
//    
//    for (int j = 1; j<=10; j++)
//    {
//        for (int i = j; i<=10; i++)
//            {
//                cout<<i << " " << endl;
//            }
//    }
//    
//    return 0;
//}


